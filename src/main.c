#include <curses.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define CTRL_KEY(k) ((k) & 0x1f)
#define BACKSPACE 127

bool readonly = 0;

char *filename;
FILE *infile;

char buf[16384];
int cur_first_line; // For scrolling
int cur_line = 1; // To not output more than (screen size - 1)
int cur_last_line; // For scrolling

bool scroll_back = 0;

int max_y;
int file_end = 0; // y value when file is ended
int max_x;

int x = 0;
int y = 0;

int j = 0; 

void writefile() {
	if (!filename) 
		mvprintw(max_y-1, 0, "ERROR: No file name.");
	else 
		mvprintw(max_y-1, 0, "Written file: %s", filename);
}

void command_check(char *command){
	if (command[0] == 'w') {
		writefile();
	} else if (command[0] == '!') {
		command[0] = 0;
		command[1] = 0;
		printw("%s", command);
		refresh();
	} else {
		mvprintw(max_y-1, 0, "Unknown command: %s", command);
		refresh();
	}
	move(x,y);
}

void readfile(FILE *file) { // Function to read file to buffer
	char ch;
	int i = 0;
	while ((ch = fgetc(file)) != EOF)
        {
        	buf[i] = ch;
		if (buf[i] == '\n') {
			file_end++;
		}
		i++;
        }
        fclose(file);
}

void printbuf() {
	clear();
	int i;
	cur_first_line = y;
	for (i = cur_first_line; i < max_y - 1; i++) {
		while (buf[j] != '\n') {
			printw("%c", buf[j]);	
			j++;
		}
		printw("\n");
		j++;
			
		if (buf[j] == '\0'){
			break;
		}
	}
	move(y, x);
	refresh();
}

WINDOW *create_newwin(int height, int width, int starty, int startx)
{
  WINDOW *local_win = newwin(height, width, starty, startx);
  box(local_win, 0, 0);
  wrefresh(local_win);

  return (local_win);
}

int main(int argc, char *argv[])
{
    	initscr();
	WINDOW *gxtwin;
	gxtwin = create_newwin(max_y, max_x, 0, 0);
    	int ch;
	raw();
	scrollok(gxtwin, TRUE);
	curs_set(TRUE);
	keypad(stdscr, TRUE);
	noecho();

 	getmaxyx(stdscr, max_y, max_x);

	cur_last_line = max_y - 1;

	if (argc >= 2) {
		filename = argv[1];
		if (readonly)
			infile = fopen(filename, "r");
		else
			infile = fopen(filename, "rw");
	        if (infile == NULL ) {
			endwin();
                        printf("Cannot find file: %s\n", argv[1]);
                        return 1;
                }

		readfile(infile);
		printbuf();
		y = 0;
		x = 0;
		move(y, x);
	} else {
		addstr("------------------\n| GRU gxt editor |\n------------------\n");
		refresh();
		y = 0;
		x = 0;
		move(y, x);
	}
	char prompt_str[50]; //FIXME
	int prompt_str_index = -1;
	bool prompt_mode = 0;


	while((ch = getch()) != CTRL_KEY('q')) {
		move(y, x);	
		switch (ch)
	        {
        	  case CTRL_KEY('b'): case KEY_LEFT:
	            x--;
		    move(y, x);
		    refresh();
        	    break;
		  case CTRL_KEY('f'): case KEY_RIGHT:
        	    x++;
		    move(y, x);
		    refresh();
	            break;
        	  case CTRL_KEY('n'): case KEY_DOWN:
	            y++;
		    move(y, x);
		    refresh();
        	    break;
	          case CTRL_KEY('p'): case KEY_UP:
        	    y--;
		    move(y, x);
		    refresh();
	            break;
		  case CTRL_KEY('e'): // FIXME: Page Down
		    if (file_end > max_y && max_y-y) {
			    y = file_end-1;
			    move(y-1,0);
			    printbuf();
		    } else {
			    y = file_end-1;
		    }
		    break;
		  case CTRL_KEY('l'):
		    scroll_back = 1;
		    printbuf();
		    break;
	        }

		if (y > file_end - 2) {
			y = file_end - 1;
			move(y, x);
		} else if (y > max_y - 2) {
			y = max_y - 1;
			move(y, x);
		} else if (y <= 0) {
			y = 0;
			move(y, x);
		} else if (x >= max_x) {
			x = max_x - 1;
			move(y, x);
		} else if (x <= 0) {
			x = 0;
			move(y, x);
		}
		
		if (y > cur_last_line-1) {
			printbuf();
			y = 0;
			x = 0;
			move(y,x);
		} 
		if (y <= cur_first_line-1 && cur_first_line > 0) {
			printbuf();
			y = max_y - 1;
			x = 0;
			move(y,x);
		}
		if (!prompt_mode) {
			prompt_str_index = 0;	
		}
		refresh();
		if (isprint(ch))
			printw("%c", buf[j]);
		if (ch == CTRL_KEY('g')) {
			if (x <= 0)
				move(y-1, x+1);
			else
				move(y, x-1);
			delch();
			refresh();
		}
		if (prompt_mode) {
			if(ch == '\n') {
				prompt_mode = 0;
				refresh();
				command_check(prompt_str);
				prompt_str_index = -1; // FIXME
				continue;
			}
			if (ch == CTRL_KEY('g') ) {
				delch();
				delch();
				refresh();
				continue;
			}
			prompt_str[prompt_str_index] = ch;
			prompt_str_index++;
		}
		refresh();
		if (ch == CTRL_KEY('r') && !prompt_mode) {
			prompt_mode = 1;
			mvprintw(max_y-1, 0, "M-r ");
			refresh();
			echo();
		} else if (ch == CTRL_KEY('w')){
			writefile();
		    	move(y, x);
		} else if (ch == CTRL_KEY('[') && prompt_mode){
			prompt_mode = 0;
			move(y,x);
			refresh();
		}
    	}
	clear();
    	endwin();

	return EXIT_SUCCESS;
}
